topo = {
  name: "rvn-test",
  nodes: [deb("test")]
}

function deb(name) {
  return {
    name: name,
    image: "debian-buster",
    cpu: { cores: 4, passthru: true },
    mounts: [{source: env.PWD+'/../../', point: '/tmp/rvn'}]
  }
}
