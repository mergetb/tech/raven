Name: raven
Version: 0.6.8
Release: 1%{?dist}
Summary: Raven Apparatus for Virtual Encodable Networks
License: Apache 2.0
URL: https://gitlab.com/mergetb/tech/raven

BuildRequires: golang bcc-devel libvirt-devel make
Requires: bcc libvirt nodejs redis ansible

%description

Raven is a tool for rapidly designing, deploying and managing virtual networks.
Raven networks are:

    - designed programatically through a javascript API
    - managed through a command line interface
    - materialized and deployed by a libvirt enabled backend

%build

make -C %{_sourcedir} -j `nproc`

%install

DESTDIR="%{buildroot}" prefix="/usr" make -C %{_sourcedir} install
ln -s node "%{buildroot}/%{_usr}/bin/nodejs"

%files

/etc/bash_completion.d/raven
/etc/raven/qemu.conf
%dir /var/rvn
/var/rvn/img
/var/rvn/ssh/rvn
/var/rvn/ssh/rvn.pub
/var/rvn/template/config.yml
/var/rvn/template/sys.exports
/var/rvn/util/iamme-freebsd
/var/rvn/util/iamme-linux
/etc/sysctl.d/raven.conf
%{_bindir}/rvn
%{_bindir}/nodejs
%{_usr}/lib/rvn/bpf_helpers.h
%{_usr}/lib/rvn/modeling.js
%{_usr}/lib/rvn/run_model.js
%{_usr}/lib/rvn/v2v.c
/var/rvn/run

%post

systemctl enable redis
systemctl start redis
