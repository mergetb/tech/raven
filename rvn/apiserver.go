package rvn

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	api "gitlab.com/mergetb/tech/raven/api"
)

func RunApiserver(endpoint string) {

	http.HandleFunc("/power", powerHandler)
	http.HandleFunc("/reboot", rebootHandler)
	log.Printf("apiserver listening on %s", endpoint)
	log.Fatal(http.ListenAndServe(endpoint, nil))

}

func rebootHandler(w http.ResponseWriter, r *http.Request) {

	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("[/reboot] error reading body: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("body read"))
	}

	var msg api.RebootMessage
	err = json.Unmarshal(buf, &msg)
	if err != nil {
		log.Printf("[/reboot] invalid body: %s: %v", string(buf), err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("malformed body"))
	}

	log.Printf("msg: %+v", msg)

	switch strings.ToLower(msg.Action) {
	case api.RebootAction:
		topo, err := LoadTopo()
		if err != nil {
			log.Fatal(err)
		}

		rr := RebootRequest{
			Topo:  topo.Name,
			Nodes: msg.Nodes,
			Reset: msg.Reset,
		}
		Reboot(rr)

	default:
		log.Printf("[/reboot] invalid action: %s: %v", msg.Action, err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid action"))
	}

}

func powerHandler(w http.ResponseWriter, r *http.Request) {

	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("[/power] error reading body: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("body read"))
	}

	var msg api.PowerMessage
	err = json.Unmarshal(buf, &msg)
	if err != nil {
		log.Printf("[/power] invalid body: %s: %v", string(buf), err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("malformed body"))
	}

	log.Printf("msg: %+v", msg)

	switch strings.ToLower(msg.Action) {
	case api.PowerOnAction:
		Powerup(msg.Nodes)
	case api.PowerOffAction:
		Powerdown(msg.Nodes, true)
	case api.PowerCycleAction:
		topo, err := LoadTopo()
		if err != nil {
			log.Fatal(err)
		}

		rr := RebootRequest{
			Topo:  topo.Name,
			Nodes: msg.Nodes,
			Reset: true,
		}
		Reboot(rr)

	default:
		log.Printf("[/power] invalid action: %s: %v", msg.Action, err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid action"))
	}

}
