package rvn

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	bpf "github.com/iovisor/gobpf/bcc"
	"github.com/libvirt/libvirt-go"
	"gitlab.com/mergetb/tech/rtnl"
)

/*
#cgo CFLAGS: -I/usr/include/bcc/compat
#cgo LDFLAGS: -lbcc
#include <bcc/bcc_common.h>
#include <bcc/libbpf.h>
void perf_reader_free(void *ptr);
*/
import "C"

func v2v(net *libvirt.Network) {

	br, err := net.GetBridgeName()
	if err != nil {
		log.Fatalf("failed to get bridge name: %v", err)
	}

	log.Printf("activating v2v on %s", br)

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		log.Fatalf("failed to open rtnl context: %v", err)
	}

	bridge, err := rtnl.GetLink(ctx, br)
	if err != nil {
		log.Fatalf("failed to get bridge link: %v", err)
	}

	_links, err := rtnl.ReadLinks(ctx, nil)
	if err != nil {
		log.Fatalf("failed to read links: %v", err)
	}

	var links []*rtnl.Link
	for _, x := range _links {
		if !strings.HasPrefix(x.Info.Name, "vnet") {
			continue
		}
		if x.Info.Master == uint32(bridge.Msg.Index) {
			links = append(links, x)
		}
	}
	if len(links) != 2 {
		for _, x := range links {
			log.Printf("%v", x.Info.Name)
		}
		log.Fatal("v2v only supported for point to point links")
	}

	v2vLinks(links[0], links[1])
	v2vLinks(links[1], links[0])

}

func findv2v() (string, error) {

	// search local and system installation paths
	paths := []string{
		"/usr/local/lib/rvn",
		"/usr/lib/rvn",
	}

	for _, p := range paths {

		f := fmt.Sprintf("%s/v2v.c", p)
		if _, err := os.Stat(f); err == nil {
			return p, nil
		} else if errors.Is(err, os.ErrNotExist) {
			continue
		} else {
			return "", err
		}
	}

	return "", fmt.Errorf("failed to find bpf program source")

}

func v2vLinks(ifx, peer *rtnl.Link) {

	path, err := findv2v()
	if err != nil {
		log.Fatal(err)
	}

	file := fmt.Sprintf("%s/v2v.c", path)
	src, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatalf("failed to read bpf program source: %v", err)
	}

	module := bpf.NewModule(string(src), []string{
		"-w",
		fmt.Sprintf("-I%s", path),
	})
	if module == nil {
		log.Fatalf("failed to create module")
	}
	defer module.Close()

	fn, err := module.Load("v2v_prog", C.BPF_PROG_TYPE_XDP, 1, 65536)
	if err != nil {
		log.Fatalf("failed to load bpf program: %v", err)
	}

	err = module.AttachXDP(ifx.Info.Name, fn)
	if err != nil {
		log.Fatalf("failed to attach bpf program to device %s: %v", ifx.Info.Name, err)
	}

	peerMap := bpf.NewTable(module.TableId("peer_map"), module)
	key, _ := peerMap.KeyStrToBytes("0")
	leaf, _ := peerMap.LeafStrToBytes(fmt.Sprintf("%d", peer.Msg.Index))
	err = peerMap.Set(key, leaf)
	if err != nil {
		log.Fatalf("failed to set peer map: %v", err)
	}

}
