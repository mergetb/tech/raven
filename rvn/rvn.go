package rvn

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"text/tabwriter"

	"github.com/gofrs/flock"
	xlibvirt "github.com/libvirt/libvirt-go-xml"
	log "github.com/sirupsen/logrus"
	//xir "gitlab.com/mergetb/xir/lang/go"
)

// Version holds the raven version string
var Version = "undefined"

const (
	// DefaultDiskBootOrder is used when one is not explicitly specified
	DefaultDiskBootOrder = 10

	TopoFile = "topo.json"

	RavenDir = "/.rvn"
)

// Types ======================================================================

// Mount describes an NFS mount point in a VM.
type Mount struct {
	Point  string `json:"point"`
	Source string `json:"source"`
}

// UnitValue describes a value that has an associated unit of measure.
type UnitValue struct {
	Value int    `json:"value"`
	Unit  string `json:"unit"`
}

// CPU describes the processor in a VM.
type CPU struct {
	Sockets  int    `json:"sockets"`
	Cores    int    `json:"cores"`
	Threads  int    `json:"threads"`
	Model    string `json:"arch"`
	Passthru bool   `json:"passthru"`
}

// Memory describes the memory capacity of a VM.
type Memory struct {
	Capacity UnitValue `json:"capacity"`
}

// Port describes a VM connection point.
type Port struct {
	Link  string
	Index int
}

// Disktype describes a VM disk.
type Disktype struct {
	Bus       string `json:"bus"`
	Dev       string `json:"dev"`
	Size      string `json:"size"`
	Source    string `json:"source"`
	BootOrder int    `json:"bootorder"`
}

type Alias struct {
	Name  string `json:"name"`
	Alias string `json:"alias"`
}

// Host describes a VM.
type Host struct {
	Name            string      `json:"name"`
	Arch            string      `json:"arch"`
	Platform        string      `json:"platform"`
	Machine         string      `json:"machine"`
	Kernel          string      `json:"kernel"`
	Cmdline         string      `json:"cmdline"`
	Initrd          string      `json:"initrd"`
	Image           string      `json:"image"`
	OS              string      `json:"os"`
	Disks           []*Disktype `json:"disks"`
	NoTestNet       bool        `json:"notestnet"`
	EBPF            bool        `json:"ebpf"`
	Mounts          []Mount     `json:"mounts"`
	CPU             *CPU        `json:"cpu,omitempty"`
	Memory          *Memory     `json:"memory,omitempty"`
	DefaultNic      string      `json:"defaultnic"`
	DefaultDisktype *Disktype   `json:"defaultdisktype"`
	SysFirmware     string      `json:"firmware,omitempty"`
	Emulator        string      `json:"emulator"`
	Iommu           bool        `json:"iommu"`
	Video           string      `json:"video"`
	DedicatedDisk   bool        `json:"dedicateddisk"`
	DiskCache       string      `json:"diskcache"`
	VirtualClock    bool        `json:"vclock"`

	TelnetPort int

	// internal use only
	ports []Port `json:"-"`
}

// Zwitch is a host, but carries the semantic that it is a switch.
type Zwitch struct {
	Host
}

// Node is a host, but carries the semantic that it is a regular node.
type Node struct {
	Host
}

// Endpoint describes a VM connection point.
type Endpoint struct {
	Name string `json:"name"`
	Port int    `json:"port"`
}

// Link descrbes a P2P link between VMs.
type Link struct {
	Name      string                 `json:"name"`
	Endpoints [2]Endpoint            `json:"endpoints"`
	Props     map[string]interface{} `json:"props"`
	AllowLACP bool                   `json:"lacp"`
	NoJumbo   bool                   `json:"nojumbo"`
	V2V       bool                   `json:"v2v"`
}

// Options contains various VM options
type Options struct {
	Display string `json:"display"`
}

// A Topo is a collection of nodes, switches an links.
type Topo struct {
	Name         string   `json:"name"`
	Nodes        []Node   `json:"nodes"`
	Switches     []Zwitch `json:"switches"`
	Links        []Link   `json:"links"`
	Dir          string   `json:"dir"`
	MgmtIP       string   `json:"mgmtip"`
	Options      Options  `json:"options"`
	SubnetOffset int      `json:"subnetoffset"`
	Aliases      []Alias  `json:"aliases"`
}

type TopoInfo struct {
	Name string
	Path string
}

// Runtime tracks raven runtime state.
type Runtime struct {
	SubnetTable        [256]bool
	SubnetReverseTable map[string]int
	TelnetPorts        []int
	Topos              map[string]TopoInfo
}

// RebootRequest is used to reboot nodes.
type RebootRequest struct {
	Topo  string   `json:"topo"`
	Nodes []string `json:"nodes"`
	Reset bool     `json:"reset"`
}

// Default Values =============================================================
//
// The default values are organized by platform. Each platform provides a basic
// set of default configuration variables sufficent to start a vm under that
// platform with no user specified configuration. Every new platform must
// support this runnable by convention model

// Platform describes the default values for a given platform
type Platform struct {
	Name      string
	Arch      string
	Machine   string
	CPU       *CPU
	Memory    *Memory
	Kernel    string
	Image     string
	Cmdline   string
	Initrd    string
	Nic       string
	Disktype  *Disktype
	Emulator  string
	DiskCache string
}

var defaults = struct {
	X86_64  *Platform
	Arm     *Platform
	Android *Platform
}{
	X86_64: &Platform{
		Name:    "x86_64",
		Arch:    "x86_64",
		Machine: "q35",
		CPU: &CPU{
			Sockets:  1,
			Cores:    1,
			Threads:  1,
			Passthru: false,
		},
		Memory:    &Memory{Capacity: UnitValue{Value: 1, Unit: "GB"}},
		Image:     "netboot",
		Nic:       "virtio",
		Disktype:  &Disktype{Dev: "vda", Bus: "virtio"},
		DiskCache: "none",
		Emulator:  "/usr/bin/qemu-system-x86_64",
	},
	Arm: &Platform{
		Name:    "arm7",
		Arch:    "armv7l",
		Machine: "vexpress-a9",
		CPU: &CPU{
			Sockets:  1,
			Cores:    1,
			Threads:  1,
			Model:    "cortex-a9",
			Passthru: false,
		},
		Memory:    &Memory{Capacity: UnitValue{Value: 1, Unit: "GB"}},
		Kernel:    "u-boot:a9",
		Image:     "raspbian:a9", //TODO s/raspbian/alpine/g
		Nic:       "virtio",
		Disktype:  &Disktype{Dev: "sda", Bus: "sd"},
		DiskCache: "none",
		Emulator:  "/usr/bin/qemu-system-arm",
	},
	Android: &Platform{
		Name:    "android",
		Arch:    "x86_64",
		Machine: "auto",
		CPU: &CPU{
			Sockets:  1,
			Cores:    1,
			Threads:  1,
			Model:    "kvm64",
			Passthru: false,
		},
		Memory:    &Memory{Capacity: UnitValue{Value: 2, Unit: "GB"}},
		Image:     "oreo",
		Nic:       "virtio",
		Disktype:  &Disktype{Dev: "vda", Bus: "virtio"},
		DiskCache: "none",
		Emulator:  "/usr/bin/qemu-system-x86_64",
	},
}

func fillInMissing(h *Host) {
	if h.Platform == "" {
		h.Platform = "x86_64"
	}

	switch h.Platform {

	case "x86_64":
		h.Arch = defaults.X86_64.Arch
		if h.Machine == "" {
			h.Machine = defaults.X86_64.Machine
		}
		applyCPUDefaults(&h.CPU, defaults.X86_64.CPU)
		applyMemoryDefaults(&h.Memory, defaults.X86_64.Memory)
		applyDisktypeDefaults(&h.DefaultDisktype, defaults.X86_64.Disktype)
		if h.Image == "" {
			h.Image = defaults.X86_64.Image
		}
		if h.DefaultNic == "" {
			h.DefaultNic = defaults.X86_64.Nic
		}
		if h.Emulator == "" {
			h.Emulator = defaults.X86_64.Emulator
		}
		if h.DiskCache == "" {
			h.DiskCache = defaults.X86_64.DiskCache
		}

	case "arm7":
		h.Arch = defaults.Arm.Arch
		if h.Machine == "" {
			h.Machine = defaults.Arm.Machine
		}
		applyCPUDefaults(&h.CPU, defaults.Arm.CPU)
		applyMemoryDefaults(&h.Memory, defaults.Arm.Memory)
		applyDisktypeDefaults(&h.DefaultDisktype, defaults.Arm.Disktype)
		if h.Image == "" {
			h.Image = defaults.Arm.Image
		}
		if h.Kernel == "" {
			h.Kernel = defaults.Arm.Kernel
		}
		if h.DefaultNic == "" {
			h.DefaultNic = defaults.Arm.Nic
		}
		if h.Emulator == "" {
			h.Emulator = defaults.Arm.Emulator
		}
		if h.DiskCache == "" {
			h.DiskCache = defaults.Arm.DiskCache
		}

	case "android":
		h.Arch = defaults.Android.Arch
		if h.Machine == "" {
			h.Machine = defaults.Android.Machine
		}
		applyCPUDefaults(&h.CPU, defaults.Android.CPU)
		applyMemoryDefaults(&h.Memory, defaults.Android.Memory)
		applyDisktypeDefaults(&h.DefaultDisktype, defaults.Android.Disktype)
		if h.Image == "" {
			h.Image = defaults.Android.Image
		}
		if h.DefaultNic == "" {
			h.DefaultNic = defaults.Android.Nic
		}
		if h.Emulator == "" {
			h.Emulator = defaults.Android.Emulator
		}
		if h.DiskCache == "" {
			h.DiskCache = defaults.Android.DiskCache
		}

	}

}

func applyCPUDefaults(to **CPU, from *CPU) {
	if *to == nil {
		*to = new(CPU)
	}
	if (*to).Model == "" {
		(*to).Model = from.Model
	}
	if (*to).Sockets == 0 {
		(*to).Sockets = from.Sockets
	}
	if (*to).Cores == 0 {
		(*to).Cores = from.Cores
	}
	if (*to).Threads == 0 {
		(*to).Threads = from.Threads
	}
}

func applyMemoryDefaults(to **Memory, from *Memory) {
	if *to == nil {
		*to = new(Memory)
	}
	if (*to).Capacity.Value == 0 || (*to).Capacity.Unit == "" {
		(*to).Capacity = from.Capacity
	}
}

func applyDisktypeDefaults(to **Disktype, from *Disktype) {
	if *to == nil {
		*to = new(Disktype)
	}
	if (*to).Dev == "" {
		(*to).Dev = from.Dev
	}
	if (*to).Bus == "" {
		(*to).Bus = from.Bus
	}
}

func findKernel(h *Host) string {

	var defaultKernel string
	switch h.Arch {
	case "armv7l":
		defaultKernel = fmt.Sprintf("/var/rvn/kernel/%s", defaults.Arm.Kernel)
	case "Android":
		defaultKernel = ""
	case "x86_64":
		defaultKernel = ""
	default:
		defaultKernel = ""
	}

	if h.Kernel == "" {
		return defaultKernel
	}

	return findResource("kernel", h.Kernel, defaultKernel)

}

func findInitrd(h *Host) string {

	var defaultInitrd string
	switch h.Arch {
	case "armv7l":
		defaultInitrd = ""
	case "Android":
		defaultInitrd = ""
	case "x86_64":
		defaultInitrd = ""
	default:
		defaultInitrd = ""
	}

	if h.Initrd == "" {
		return defaultInitrd
	}

	return findResource("initrd", h.Initrd, defaultInitrd)

}

func findSysFirmware(h *Host) *xlibvirt.DomainLoader {

	if h.SysFirmware == "" {
		return nil
	}

	r := findResource("firmware", h.SysFirmware, "")

	if r == "" {
		return nil
	}
	return &xlibvirt.DomainLoader{Path: r}

}

func findResource(kind, target, defaultValue string) string {

	// first: try to find the referenced resource as an absolute path
	_, err := os.Stat(fmt.Sprintf(target))
	if err == nil {
		return target
	}

	// second: look locally
	wd, err := os.Getwd()
	if err != nil {
		log.Errorf(
			"find-%s: error getting working directory - using default", kind)
		return defaultValue
	}
	_, err = os.Stat(fmt.Sprintf("%s/%s", wd, target))
	if err == nil {
		return fmt.Sprintf("%s/%s", wd, target)
	}

	// third: if we cant find the referenced resource locally, try to find the
	// it in the rvn installation directory
	_, err = os.Stat(fmt.Sprintf("/var/rvn/%s/%s", kind, target))
	if err == nil {
		return fmt.Sprintf("/var/rvn/%s/%s", kind, target)
	}

	log.Warnf(
		"find-%s: '%s' not found - using default", kind, target)

	return defaultValue
}

// Methods ====================================================================

// Topo -----------------------------------------------------------------------

// GetHost gets a host by name from a topology.
func (t *Topo) GetHost(name string) *Host {
	for i, x := range t.Nodes {
		if x.Name == name {
			return &t.Nodes[i].Host
		}
	}
	for i, x := range t.Switches {
		if x.Name == name {
			return &t.Switches[i].Host
		}
	}
	return nil
}

// GetLink gets a link by name from a topology.
func (t *Topo) GetLink(name string) *Link {
	for i, x := range t.Links {
		if x.Name == name {
			return &t.Links[i]
		}
	}
	return nil
}

// QualifyName returns the topology qualified name of a host, link or switch.
func (t Topo) QualifyName(n string) string {
	return t.Name + "_" + n
}

// String returns the string representation of a topology.
func (t Topo) String() string {
	s := t.Name + "\n"
	s += "nodes" + "\n"
	for _, v := range t.Nodes {
		s += fmt.Sprintf("  %+v\n", v)
	}
	s += "switches" + "\n"
	for _, v := range t.Switches {
		s += fmt.Sprintf("  %+v\n", v)
	}
	s += "links" + "\n"
	for _, v := range t.Links {
		s += fmt.Sprintf("  %+v\n", v)
	}
	return s
}

// Runtime --------------------------------------------------------------------

// Save persists the runtime to the state file.
func (r *Runtime) Save() {
	data, err := json.MarshalIndent(r, "", "  ")
	if err != nil {
		log.Errorf("unable to marshal runtime state - %v", err)
		return
	}

	fileLock := flock.New("/var/rvn/run")
	err = fileLock.Lock()
	if err != nil {
		log.Fatal("failed to lock runtime directory")
	}
	defer fileLock.Unlock()

	err = ioutil.WriteFile("/var/rvn/run", []byte(data), 0644)
	if err != nil {
		log.Errorf("error saving runtime state - %v", err)
	}
}

// LoadRuntime loads the runtime from the state file.
func LoadRuntime() *Runtime {

	fileLock := flock.New("/var/rvn/run")
	err := fileLock.Lock()
	if err != nil {
		log.Fatal("failed to lock runtime directory")
	}
	defer fileLock.Unlock()

	data, err := ioutil.ReadFile("/var/rvn/run")
	if err != nil {
		log.Fatalf("error reading rvn runtime file - %v", err)
	}

	rt := &Runtime{}
	err = json.Unmarshal(data, rt)
	if err != nil {
		log.Fatalf("error decoding runtime config - %v", err)
	}
	if rt.SubnetReverseTable == nil {
		rt.SubnetReverseTable = make(map[string]int)
	}
	return rt
}

func (r *Runtime) RecordTopology(info TopoInfo) {

	if r.Topos == nil {
		r.Topos = make(map[string]TopoInfo)
	}
	r.Topos[info.Name] = info
	r.Save()

}

func (r *Runtime) RemoveTopology(name string) {

	if r.Topos == nil {
		return
	}

	delete(r.Topos, name)
	r.Save()

}

// AllocateSubnet allocates a new subnet for a topology, updating the subnet
// allocation table.
func (r *Runtime) AllocateSubnet(tag string) int {
	i, ok := r.SubnetReverseTable[tag]
	if ok {
		return i
	}
	for i, b := range r.SubnetTable {
		if !b {
			r.SubnetTable[i] = true
			r.SubnetReverseTable[tag] = i
			r.Save()
			return i
		}
	}
	return -1
}

// FreeSubnet frees a subnet from the subnet allocation table.
func (r *Runtime) FreeSubnet(tag string) {
	i, ok := r.SubnetReverseTable[tag]
	if ok {
		r.SubnetTable[i] = false
		delete(r.SubnetReverseTable, tag)
		r.Save()
	}
}

// NewTelnetPort allocates a new telnet port for a VM.
func (r *Runtime) NewTelnetPort() int {

	port := nextIntFrom(r.TelnetPorts, 4000)
	r.TelnetPorts = append(r.TelnetPorts, port)
	r.Save()

	return port

}

// FreeTelnetPort frees a telnet port, making it available again.
func (r *Runtime) FreeTelnetPort(port int) {

	idx := -1
	for i, x := range r.TelnetPorts {
		if x == port {
			idx = i
			break
		}
	}

	if idx != -1 {
		r.TelnetPorts = remove(r.TelnetPorts, idx)
	} else {
		log.Warnf("request to remove unknown telnet port %d", port)
	}

	r.Save()

}

func remove(s []int, i int) []int {
	s[len(s)-1], s[i] = s[i], s[len(s)-1]
	return s[:len(s)-1]
}

// Topo -----------------------------------------------------------------------

func (l *Link) IsExternal() bool {

	external, ok := l.Props["external"]
	if ok {
		ext, ok := external.(bool)
		if ok && ext {
			return true
		}
	}

	return false

}

func (l *Link) IsTap() bool {
	tap, ok := l.Props["tap"]

	if ok {
		ext, ok := tap.(bool)
		if ok && ext {
			return true
		}
	}

	return false
}

func (l *Link) GetBridge() (string, error) {
	bridge, ok := l.Props["bridge"].(string)
	if !ok {
		return "", fmt.Errorf("link has no prop `bridge`")
	}
	return bridge, nil
}

// Functions ==================================================================

// RunModel executes a users raven model. Required images are downloaded.
// Libvirt specs are generated. Virtual disks are created. Table allocations are
// made.
func RunModel() error {

	// execute the javascript model
	out, err := exec.Command(
		"nodejs",
		"/usr/lib/rvn/run_model.js",
		"model.js",
	).CombinedOutput()

	if err != nil {
		log.Errorf("error running model")
		log.Errorf(string(out))
		return err
	}

	// save the result of the model execution in the working directory
	topo, err := ReadTopo(out)
	if err != nil {
		log.Errorf("error reading topo %v", err)
		return err
	}
	wd, err := os.Getwd()
	if err != nil {
		log.Errorf("cannot determine working directory %v", err)
		return err
	}
	topo.Dir = wd

	// add compute the telnet ports for each node and switch
	for i := range topo.Nodes {
		topo.Nodes[i].TelnetPort = LoadRuntime().NewTelnetPort()
	}
	for i := range topo.Switches {
		topo.Switches[i].Host.TelnetPort = LoadRuntime().NewTelnetPort()
	}

	// apply defaults to any values not supplied by user
	for i := 0; i < len(topo.Nodes); i++ {
		fillInMissing(&topo.Nodes[i].Host)
	}
	for i := 0; i < len(topo.Switches); i++ {
		fillInMissing(&topo.Switches[i].Host)
	}

	err = BuildRvnDisks(topo)
	if err != nil {
		return err
	}

	err = SaveTopo(topo)
	return err
}

// SaveTopo persists the json representation of a topo to the .rvn dir.
func SaveTopo(t Topo) error {
	wd, err := WkDir()
	if err != nil {
		log.Errorf("SaveTopo: could not determine working directory")
		return err
	}
	path := wd + "/" + TopoFile

	data, err := json.MarshalIndent(&t, "", "  ")
	if err != nil {
		log.Errorf("unable to marshal topology - %v", err)
		return err
	}

	err = ioutil.WriteFile(path, []byte(data), 0644)
	if err != nil {
		log.Errorf("error saving topology state - %v", err)
		return err
	}
	return nil
}

// LoadTopo loads a topology from the json representation in the .rvn dir.
func LoadTopo() (Topo, error) {

	wd, err := WkDir()
	if err != nil {
		log.Errorf("loadtopo: could not determine working directory")
		return Topo{}, err
	}
	path := wd + "/" + TopoFile
	return LoadTopoFile(path)
}

func RecordTopo() {

	topo, err := LoadTopo()
	if err != nil {
		log.Fatal(err)
	}

	wd, err := WkDir()
	if err != nil {
		log.Errorf("loadtopo: could not determine working directory")
	}

	LoadRuntime().RecordTopology(TopoInfo{
		Name: topo.Name,
		Path: strings.TrimSuffix(wd, RavenDir),
	})

}

func RemoveTopo() {

	topo, err := LoadTopo()
	if err != nil {
		log.Fatal(err)
	}

	LoadRuntime().RemoveTopology(topo.Name)

}

func ListTopo() {

	var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
	fmt.Fprintf(tw, "TOPOLOGY\tPATH\n")
	for _, x := range LoadRuntime().Topos {
		fmt.Fprintf(tw, "%s\t%s\n", x.Name, x.Path)
	}
	tw.Flush()

}

// LoadTopoFile loads a topology from a json representation file at a specified
// path.
func LoadTopoFile(path string) (Topo, error) {

	f, err := ioutil.ReadFile(path)
	if err != nil {
		return Topo{}, err
	}
	topo, err := ReadTopo(f)
	if err != nil {
		return Topo{}, err
	}
	return topo, nil

}

// WkDir returns the working dir of a raven topology
func WkDir() (string, error) {
	wd, err := os.Getwd()
	if err != nil {
		log.Errorf("wkdir: could not determine working directory %v", err)
		return "", err
	}
	return wd + RavenDir, nil
}

// SrcDir returns the source dir of a raven topology
func SrcDir() (string, error) {
	wd, err := os.Getwd()
	if err != nil {
		log.Errorf("srcdir: could not determine working directory %v", err)
		return "", err
	}
	return wd, nil
}

// ReadTopo reads the supplied data as a json representation of a raven
// topology.
func ReadTopo(src []byte) (Topo, error) {
	var topo Topo
	err := json.Unmarshal(src, &topo)
	if err != nil {
		return topo, err
	}
	return topo, nil
}

// Rvn2Xir transforms a raven topology into a MergeTB XIR topology.
/* TODO port to XIR v0.3
func Rvn2Xir(t *Topo) *xir.Net {

	net := xir.NewNet()

	for _, x := range t.Nodes {
		net.Node().Set(xir.Props{"name": x.Name})
	}
	for _, x := range t.Switches {
		net.Node().Set(xir.Props{"name": x.Name})
	}
	for _, x := range t.Links {
		_, _, a := net.GetNodeByName(x.Endpoints[0].Name)
		if a == nil {
			log.Errorf("bad node name %s", x.Endpoints[0].Name)
			continue
		}
		_, _, b := net.GetNodeByName(x.Endpoints[1].Name)
		if b == nil {
			log.Errorf("bad node name %s", x.Endpoints[1].Name)
			continue
		}
		net.Link(a.Endpoint(), b.Endpoint())
	}

	return net
}
*/

// BuildRvnDisks fills in missing disks, adding disks that do not exist.
func BuildRvnDisks(topo Topo) error {
	for _, host := range topo.Nodes {
		for j, x := range host.Disks {
			if x.Source == "" {
				if x.Size == "" {
					return fmt.Errorf("disk source and size not specified, one must be")
				}
				sourceName := fmt.Sprintf("%s-disk%d", host.Name, j)
				name, err := createSource(x.Size, sourceName)
				if err != nil {
					return err
				}
				x.Source = name
			}
		}
	}
	return nil
}

func createSource(size, name string) (string, error) {

	wd, err := WkDir()
	if err != nil {
		return "", err
	}
	diskName := wd + "/" + name
	out, err := exec.Command(
		"qemu-img",
		"create",
		"-f",
		"qcow2",
		"-o", "lazy_refcounts=on,preallocation=falloc",
		diskName,
		size).CombinedOutput()
	if err != nil {
		log.Errorf("%v", err)
		log.Errorf("%s", out)
		return "", fmt.Errorf("error creating image file for %s", diskName)
	}
	return diskName, nil
}
