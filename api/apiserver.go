package rvnapi

const (
	PowerOnAction    = "on"
	PowerOffAction   = "off"
	PowerCycleAction = "cycle"
	RebootAction     = "reboot"
)

type PowerMessage struct {
	Action string
	Nodes  []string
}

type RebootMessage struct {
	Action string
	Nodes  []string
	Reset  bool
}
