package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"github.com/fatih/color"
	"github.com/mergetb/go-ping"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/mergetb/tech/raven/rvn"
	//"gitlab.com/mergetb/xir/tools/viz"
)

var (
	verbose        bool
	showBridgeTaps bool
)

func main() {

	log.SetLevel(log.InfoLevel)

	var versionCmd = &cobra.Command{
		Use:   "version",
		Short: "Print version number",
		Run: func(cmd *cobra.Command, args []string) {
			printVersion()
		},
	}

	var buildCmd = &cobra.Command{
		Use:   "build",
		Short: "Build raven topology",
		Long:  "Build raven topology. Create VM definitions, images, and networks.",
		Run: func(cmd *cobra.Command, args []string) {
			doBuild()
		},
	}

	var deployCmd = &cobra.Command{
		Use:   "deploy",
		Short: "Deploy topology",
		Long:  "Deploy topology by starting up VMs and creating networks.  Requires `build` first.",
		Run: func(cmd *cobra.Command, args []string) {
			doDeploy()
		},
	}

	var configureCmd = &cobra.Command{
		Use:   "configure",
		Short: "Configure topology",
		Long:  "Configure topology. Runs ansible scripts in config dir.  Requires `deploy` first.",
		Run: func(cmd *cobra.Command, args []string) {
			doConfigure(args)
		},
	}

	var shutdownCmd = &cobra.Command{
		Use:   "shutdown",
		Short: "Shutdown topology",
		Long:  "Shutdown topology. Stops the raven instances, requires `deploy` to restart.",
		Run: func(cmd *cobra.Command, args []string) {
			doShutdown()
		},
	}

	var destroyCmd = &cobra.Command{
		Use:   "destroy",
		Short: "Destroy topology",
		Long:  "Destroy topology.  Tears down entire topology, must re-run `build`.",
		Run: func(cmd *cobra.Command, args []string) {
			doDestroy()
		},
	}

	var statusCmd = &cobra.Command{
		Use:   "status",
		Short: "Get deployment and configuration status of topology",
		Run: func(cmd *cobra.Command, args []string) {
			doStatus()
		},
	}
	statusCmd.Flags().BoolVarP(&showBridgeTaps, "bridge", "b", false, "show bridge taps")

	/*
		var vizCmd = &cobra.Command{
			Use:   "viz",
			Short: "Vizualize topology",
			Run: func(cmd *cobra.Command, args []string) {
				doViz()
			},
		}
	*/

	var ipCmd = &cobra.Command{
		Use:   "ip <vm>",
		Short: "Display a VM's ip address",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doIP(args[0])
		},
	}

	var sshCmd = &cobra.Command{
		Use:   "ssh <vm>",
		Short: "Get a VM's command",
		Long:  "Get a VM's SSH command. To use: eval $(rvn ssh node1)",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doSSH(args[0])
		},
	}

	var vncCmd = &cobra.Command{
		Use:   "vnc <vm>",
		Short: "Show a VM's VNC port",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doVnc(args[0])
		},
	}

	var telnetCmd = &cobra.Command{
		Use:   "telnet <vm>",
		Short: "Show a VM's telnet port",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doTelnet(args[0])
		},
	}

	var ansibleCmd = &cobra.Command{
		Use:   "ansible <vm> <playbook>",
		Short: "Run an Ansible playbook on a VM",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			doAnsible(args[0], args[1])
		},
	}

	var soft bool
	var rebootCmd = &cobra.Command{
		Use:   "reboot <vm1> [vm2 vm3 ...]",
		Short: "Reboot VMs",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doReboot(args, !soft)
		},
	}
	rebootCmd.Flags().BoolVarP(
		&soft, "soft", "s", false, "send ACPI signal rather than power cycle")

	var powerupCmd = &cobra.Command{
		Use:   "powerup <vm1> [vm2 vm3 ...]",
		Short: "powerup VMs",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doPowerup(args)
		},
	}

	var force bool
	var powerdownCmd = &cobra.Command{
		Use:   "powerdown <vm1> [vm2 vm3 ...]",
		Short: "powerdown VMs",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doPowerdown(args, force)
		},
	}
	powerdownCmd.Flags().BoolVarP(
		&force, "force", "f", false, "pull the plug instead of sending ACPI signal")

	var pingwaitCmd = &cobra.Command{
		Use:   "pingwait <vm1> [vm2 vm3 ...]",
		Short: "Ping VM node until online",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doPingwait(args)
		},
	}

	var wipeCmd = &cobra.Command{
		Use:   "wipe <vm1> [vm2 vm3 ...]",
		Short: "Wipe VM",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doWipe(args)
		},
	}

	var list = &cobra.Command{
		Use:   "list",
		Short: "List active topologies",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			rvn.ListTopo()
		},
	}

	var serverEndpoint string
	var apiserverCmd = &cobra.Command{
		Use:   "apiserver",
		Short: "Run Raven API server",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			rvn.RunApiserver(serverEndpoint)
		},
	}
	apiserverCmd.Flags().StringVarP(
		&serverEndpoint, "serve", "s", ":9316", "server endpoint")

	var rootCmd = &cobra.Command{
		Use: "rvn",
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			if verbose {
				log.SetLevel(log.DebugLevel)
				log.Infof("Set log level to %s", log.GetLevel())
			}
		},
	}
	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Increase verbosity")

	var completionCmd = &cobra.Command{
		Use:   "autocomplete",
		Short: "Generates bash completion scripts",
		Run: func(cmd *cobra.Command, args []string) {
			rootCmd.GenBashCompletion(os.Stdout)
		},
	}
	rootCmd.AddCommand(completionCmd)

	rootCmd.AddCommand(buildCmd)
	rootCmd.AddCommand(configureCmd)
	rootCmd.AddCommand(deployCmd)
	rootCmd.AddCommand(destroyCmd)
	rootCmd.AddCommand(shutdownCmd)
	rootCmd.AddCommand(statusCmd)
	rootCmd.AddCommand(versionCmd)
	//rootCmd.AddCommand(vizCmd)
	rootCmd.AddCommand(list)

	rootCmd.AddCommand(ansibleCmd)
	rootCmd.AddCommand(ipCmd)
	rootCmd.AddCommand(pingwaitCmd)
	rootCmd.AddCommand(rebootCmd)
	rootCmd.AddCommand(sshCmd)
	rootCmd.AddCommand(vncCmd)
	rootCmd.AddCommand(telnetCmd)
	rootCmd.AddCommand(wipeCmd)
	rootCmd.AddCommand(powerupCmd)
	rootCmd.AddCommand(powerdownCmd)
	rootCmd.AddCommand(apiserverCmd)

	checkDir()
	rootCmd.Execute()
}

func doBuild() {
	checkDir()

	wd, err := rvn.WkDir()
	if err != nil {
		log.Fatal(err)
	}

	_, err = os.Stat(wd + "/" + rvn.TopoFile)
	if err == nil {
		log.Fatalf(
			"raven topology already built, run `rvn destroy` first before trying again")
	}

	err = rvn.RunModel()
	if err != nil {
		log.Fatal(err)
	}

	//error would happen in RunModel if LoadTopo failed
	topo, err := rvn.LoadTopo()
	if err != nil {
		log.Fatal(err)
	}
	err = checkRvnImages(topo)
	if err != nil {
		log.Fatal(err)
	}

	rvn.Create()
}

func doStatus() {

	status := rvn.Status()
	if status == nil {
		return
	}
	nodes := status["nodes"].(map[string]rvn.DomStatus)
	switches := status["switches"].(map[string]rvn.DomStatus)
	links := status["links"].(map[string]rvn.LinkStatus)

	log.Println(blue("nodes"))
	for _, n := range nodes {
		log.Println(domString(n))
	}
	log.Println(blue("switches"))
	for _, s := range switches {
		log.Println(domString(s))
	}

	bridgeTaps := make(map[string][]string)

	log.Println(blue("external links"))
	for _, l := range links {
		if l.Link.IsExternal() {
			log.Printf("  %s[%d] -> %s", l.Link.Endpoints[0].Name, l.Link.Endpoints[1].Port, l.Bridge)
		}
		if l.Link.IsTap() && showBridgeTaps {
			bridge, err := l.Link.GetBridge()
			if err != nil {
				log.Errorf("failed to get bridge name for tap: %s", l.Link.Name)
				continue
			}
			bridgeTaps[bridge] = append(bridgeTaps[bridge], l.Link.Name)
		}
	}

	if showBridgeTaps {
		log.Println(blue("bridge taps"))
		for bridge, taps := range bridgeTaps {
			log.Println(white(fmt.Sprintf("  %s", bridge)))
			for _, tap := range taps {
				log.Printf("    %s", tap)
			}
		}
	}
}

func doConfigure(args []string) {
	if len(args) == 0 {
		rvn.Configure(true)
	} else {
		topo, err := rvn.LoadTopo()
		if err != nil {
			log.Fatal(err)
		}

		rvn.ConfigureNodes(topo, args)
	}
}

func doDeploy() {

	errors := rvn.Launch()
	if len(errors) != 0 {
		for _, e := range errors {
			log.Println(e)
		}
		os.Exit(1)
	}

	rvn.RecordTopo()

}

func doShutdown() {

	errs := rvn.Shutdown()
	if errs != nil {
		for _, e := range errs {
			log.Printf("%v", e)
		}
		os.Exit(1)
	}

}

func doDestroy() {

	rvn.Destroy()

}

func doSSH(node string) {

	topo, err := rvn.LoadTopo()
	if err != nil {
		log.Fatal(err)
	}

	ds, err := rvn.DomainStatus(topo.Name, node)
	if err != nil {
		log.Fatalf("error getting node status %v\n", err)
	}

	h := topo.GetHost(node)
	if h == nil {
		log.Fatalf("node '%s' does not exist", node)
	}

	user := "rvn"
	if h.OS == "onie" {
		user = "root"
	}

	fmt.Printf(
		"ssh -o StrictHostKeyChecking=no -i /var/rvn/ssh/rvn %s@%s\n", user, ds.IP)

}

/*
func doViz() {

	topo, err := rvn.LoadTopo()
	if err != nil {
		log.Fatal(err)
	}

	x := rvn.Rvn2Xir(&topo)

	viz.NetSvg(topo.Name, x)

}
*/

func doIP(node string) {

	topo, err := rvn.LoadTopo()
	if err != nil {
		log.Fatal(err)
	}

	ds, err := rvn.DomainStatus(topo.Name, node)
	if err != nil {
		log.Fatalf("error getting node status %v\n", err)
	}

	fmt.Printf("%s\n", ds.IP)

}

func doVnc(node string) {

	topo, err := rvn.LoadTopo()
	if err != nil {
		log.Fatal(err)
	}

	di, err := rvn.DomainInfo(topo.Name, node)
	if err != nil {
		log.Fatalf("error getting domain info %v\n", err)
	}

	for _, x := range di.Devices.Graphics {
		if x.VNC != nil {
			fmt.Printf("%d\n", x.VNC.Port)
			break
		}
	}

}

func doTelnet(node string) {

	topo, err := rvn.LoadTopo()
	if err != nil {
		log.Fatal(err)
	}

	di, err := rvn.DomainInfo(topo.Name, node)
	if err != nil {
		log.Fatalf("error getting domain info %v\n", err)
	}

	for _, x := range di.Devices.Serials {
		if x.Source.TCP != nil {
			fmt.Printf("%s\n", x.Source.TCP.Service)
			break
		}
	}

}

func doAnsible(node, yml string) {

	topo, err := rvn.LoadTopo()
	if err != nil {
		log.Fatal(err)
	}

	sys := topo.Name

	var h *rvn.Host
	for _, x := range topo.Nodes {
		if x.Name == node {
			h = &x.Host
			break
		}
	}
	if h == nil {
		for _, x := range topo.Switches {
			if x.Name == node {
				h = &x.Host
				break
			}
		}
	}
	if h == nil {
		log.Fatalf("%s not found in topology", node)
	}

	ds, err := rvn.DomainStatus(sys, node)
	if err != nil {
		log.Fatalf("error getting node status %v\n", err)
	}

	extraVars := "ansible_become_pass=rvn"
	if strings.ToLower(h.OS) == "freebsd" {
		extraVars += " ansible_python_interpreter='/usr/local/bin/python2'"
	}

	cmd := exec.Command(
		"ansible-playbook",
		"-i", ds.IP+",",
		yml,
		"--extra-vars", extraVars,
		`--ssh-extra-args='-i/var/rvn/ssh/rvn'`,
		"--user=rvn", "--private-key=/var/rvn/ssh/rvn",
	)
	cmd.Env = append(os.Environ(), "ANSIBLE_HOST_KEY_CHECKING=False")

	reader, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatalf("failed to get stdout pipe %v", err)
	}
	scanner := bufio.NewScanner(reader)
	go func() {
		for scanner.Scan() {
			log.Printf("%s\n", scanner.Text())
		}
	}()

	err = cmd.Start()
	if err != nil {
		log.Fatalf("failed to start ansible command %v", err)
	}

	err = cmd.Wait()
	if err != nil {
		log.Fatalf("failed to wait for ansible command to finish %v", err)
	}

}

func doReboot(args []string, reset bool) {

	topo, err := rvn.LoadTopo()
	if err != nil {
		log.Fatal(err)
	}

	rr := rvn.RebootRequest{
		Topo:  topo.Name,
		Nodes: args[0:],
		Reset: reset,
	}

	rvn.Reboot(rr)

}

func doPowerup(args []string) {

	rvn.Powerup(args)

}

func doPowerdown(args []string, force bool) {

	rvn.Powerdown(args, force)

}

func doPingwait(args []string) {

	ipmap := make(map[string]string)

	// first wait until everything we need to ping has an IP
	success := false
	for !success {

		success = true

		status := rvn.Status()
		if status == nil {
			log.Fatal("could not query libvirt status")
		}

		nodes := status["nodes"].(map[string]rvn.DomStatus)
		switches := status["switches"].(map[string]rvn.DomStatus)
		// merge the switches into nodes since they can be treated the same in this
		// context
		for k, v := range switches {
			nodes[k] = v
		}

		for _, x := range args {
			n, ok := nodes[x]
			if !ok {
				log.Fatalf("%s does not exist", x)
			}
			if n.IP == "" {
				success = false
				break
			} else {
				ipmap[x] = n.IP
			}
		}

	}

	// now try to ping everything
	success = false

	for !success {

		success = true
		for _, x := range args {
			up := doPing(ipmap[x])
			if up {
				log.Infof("%s: Is UP", x)
			}
			success = success && up
		}
	}

}

func doPing(host string) bool {
	p, err := ping.NewPinger(host)
	if err != nil {
		log.Fatal(err)
	}
	p.Count = 2
	p.Timeout = time.Millisecond * 500
	p.Interval = time.Millisecond * 50
	pings := 0
	p.OnRecv = func(pkt *ping.Packet) {
		pings++
	}
	p.Run()

	return pings == 2

}

func doWipe(args []string) {

	topo, err := rvn.LoadTopo()
	if err != nil {
		log.Fatal(err)
	}

	for _, x := range args {
		err = rvn.WipeNode(topo, x)
		if err != nil {
			log.Printf("%v", err)
		}
	}

}

func checkDir() {
	wd, err := rvn.WkDir()
	if err != nil {
		log.Fatal(err)
	}

	err = os.MkdirAll(wd, 0755)
	if err != nil {
		log.Fatal(err)
	}
}

func domString(ds rvn.DomStatus) string {
	state := ds.State
	if state == "running" {
		state = green(state)
	}
	return fmt.Sprintf(
		"  %s %s %s %s", ds.Name, state, yellow(ds.ConfigState), ds.IP)
}

var white = color.New(color.FgWhite).SprintFunc()
var blueb = color.New(color.FgBlue, color.Bold).SprintFunc()
var blue = color.New(color.FgBlue).SprintFunc()
var cyan = color.New(color.FgCyan).SprintFunc()
var cyanb = color.New(color.FgCyan, color.Bold).SprintFunc()
var greenb = color.New(color.FgGreen, color.Bold).SprintFunc()
var green = color.New(color.FgGreen).SprintFunc()
var red = color.New(color.FgRed).SprintFunc()
var redb = color.New(color.FgRed, color.Bold).SprintFunc()
var yellow = color.New(color.FgYellow).SprintFunc()
var bold = color.New(color.Bold).SprintFunc()

func checkRvnImages(topo rvn.Topo) error {
	var images []string
	// NOTE: moving over to an extentionless naming scheme
	// for each Node, get the image required
	for i := range topo.Nodes {
		var currentImg string = topo.Nodes[i].Host.Image
		// no way to check existance
		exists := false
		for j := range images {
			if currentImg == images[j] {
				exists = true
				break
			}
		}
		// we did not find image, add it to our image list
		if !exists {
			images = append(images, currentImg)
		}
	}
	for i := range topo.Switches {
		var currentImg string = topo.Switches[i].Host.Image
		// no way to check existance
		exists := false
		for j := range images {
			if currentImg == images[j] {
				exists = true
				break
			}
		}
		// we did not find image, add it to our image list
		if !exists {
			images = append(images, currentImg)
		}
	}

	// for each unique image, check that it exists, if not, download it
	for i := range images {
		// parse the uri reference (with golang url parser)
		parsedURL, err := rvn.ValidateURL(images[i])
		if err != nil {
			return err
		}

		// NOTE: local images need to be prefixed with absolute path or ./
		remoteHost := parsedURL.Host

		// there is no remote host, so the image is local
		if remoteHost == "" {
			// very first thing, check if url is nil or empty, then we need to create
			// a netboot image if it does not exist
			if parsedURL.String() == "" || parsedURL.String() == "netboot" {
				err := rvn.CreateNetbootImage()
				if err != nil {
					return err
				}
				break
			}
			// check if this is local image path, or deterlab path
			splitPath := strings.Split(images[i], "/")
			// a local image must be prefixed with atleast one slash, / or ./, therefore
			// we can check the length of of the split on / to determin if deterlab or local image
			if len(splitPath) > 1 {
				// place user images in /var/rvn/img/user
				filePath := filepath.Join("/var/rvn/img/user/", splitPath[len(splitPath)-1])
				_, err := os.Stat(filePath)
				// if file exists, copy file to /var/rvn/img
				if err != nil {
					_, err = os.Stat(images[i])
					if err != nil {
						return err
					}
					// no error, so image does exist, so lets copy from path to /var/rvn/img
					log.Infof("Downloading from: " + images[i] + " to: " + filePath)
					err = rvn.CopyLocalFile(images[i], filePath)
					if err != nil {
						return err
					}
				}
				// if len == 1, we are given a name, we assume the named image is hosted on deterlab
			} else {
				// official images go in /var/rvn/img
				filePath := filepath.Join("/var/rvn/img/", images[i])
				_, err := os.Stat(filePath)
				// if there is an err, file does not exist, download it
				if err != nil {
					remotePath := "https://mirror.deterlab.net/rvn/img/" + images[i]
					log.Infof("copying : " + remotePath + " to: " + filePath)
					dlErr := rvn.DownloadFile(filePath, remotePath)
					// we tried to find the image on deterlab mirror, but could not, error
					if dlErr != nil {
						return err
					}
				}
			}
			// if the host is remote, we should assume we are going off-world to get image
		} else {
			subPath, imageName, err := rvn.ParseURL(parsedURL)
			if err != nil {
				return err
			}
			filePath := filepath.Join("/var/rvn/img/user/", subPath)
			_, err = os.Stat(filepath.Join(filePath, imageName))
			// path to image does not exist, we will need to download it
			if err != nil {
				// first we create all the subdirectorys in the file path
				crErr := os.MkdirAll(filePath, 0755)
				if crErr != nil {
					return err
				}
				// now try to download the image to the correct lcoation
				log.Infof("copying: " + parsedURL.String() + " to: " + filePath + imageName)
				dlErr := rvn.DownloadURL(parsedURL, filePath, imageName)
				if dlErr != nil {
					return err
				}
			}
		}
	}
	return nil
}

func printVersion() {
	fmt.Println(rvn.Version)
}
