// SPDX-License-Identifier: GPL-2.0
/* Copyright(c) 2019 - The Raven Authors */

#define KBUILD_MODNAME "v2v"

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 15, 0)
#include <uapi/linux/bpf.h>
#else
#include <linux/bpf.h>
#endif

#include "bpf_helpers.h"

BPF_TABLE("array", int, int, peer_map, 1);

int v2v_prog(struct xdp_md *ctx)
{

  int key = 0;
  int *peer = peer_map.lookup(&key);
  if (!peer) {
    return XDP_PASS;
  }

  return bpf_redirect(*peer, 0);
}
